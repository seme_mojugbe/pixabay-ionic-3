import { NgModule } from '@angular/core';
import { ImagePreloader } from './image-preloader';

@NgModule({
  declarations: [
    ImagePreloader
  ],
  exports: [
    ImagePreloader
  ],
})
export class ImagePreloaderModule {}
