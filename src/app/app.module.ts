import { NgModule, ErrorHandler, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePageModule } from '../pages/home/home.module';
import { PicturePageModule } from '../pages/picture/picture.module';


import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { PicturePage } from '../pages/picture/picture';
// import { BookmarksPage } from '../pages/bookmarks/bookmarks';
import { ImagePreloaderModule } from '../components/image-preloader.module';

import { NativeStorage } from '@ionic-native/native-storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Screenshot } from '@ionic-native/screenshot';
import { HttpModule } from '@angular/http';

enableProdMode();

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    // HomePage,
    TabsPage,
    // PicturePage,
    // BookmarksPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HomePageModule,
    PicturePageModule,
    ImagePreloaderModule,
    IonicModule.forRoot(MyApp, {
      isDesktop : <boolean> true
    })
  ],
  exports : [
    // ImagePreloaderModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    PicturePage,
    // BookmarksPage
  ],
  providers: [
    StatusBar,
    Screenshot,
    NativeStorage,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {

}
