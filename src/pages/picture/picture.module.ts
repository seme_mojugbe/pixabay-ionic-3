import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PicturePage } from './picture';
import { ImagePreloaderModule } from '../../components/image-preloader.module';

@NgModule({
  declarations: [
    PicturePage,
  ],
  imports: [
    IonicPageModule.forChild(PicturePage),
    ImagePreloaderModule
  ],
})
export class PicturePageModule {}
