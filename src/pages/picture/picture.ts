import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Config } from 'ionic-angular';
import { Http, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';
import { PictureBookmark } from './picture.interface';
import { Screenshot } from '@ionic-native/screenshot';

/**
 * Generated class for the PicturePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-picture',
  templateUrl: 'picture.html',
})
export class PicturePage {
  pictureID : string;
  picture : any;
  processing : boolean = true;
  imageStack : PictureBookmark[] = [];
  related : any;
  quotes : any;
  pictureTags : string[];
  screen : any;
  state : boolean = false;
  isDesktop : boolean = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http : Http,
    private nativeStorage : NativeStorage,
    private screenshot: Screenshot,
    public config : Config
  ) {
    this.pictureID = this.navParams.get('id');
    console.log('passed key is : ', this.pictureID);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PicturePage');
    this.fetchImageMeta(this.pictureID);
    this.isDesktop = this.config.get('isDesktop');
  }

  takeScreenshot(){
    this.screenshot.save('jpg', 80, 'myscreenshot.jpg').then(
      (res) => {
        this.screen = res.filePath;
        console.log('Success');
      },
      (error) => {
        console.error(error);
      }
    );
  }

  openPic(key: any) {
    this.navCtrl.push(PicturePage, {
      id : key
    });
  }

  savePicture() {

    this.imageStack.push({
      image : 'samsonite.jpg',
      user : 'Sarah'
    });
    console.log('Updated Stack: ',this.imageStack);
    this.nativeStorage.setItem('myPictures', this.imageStack).then(
      () => {
        console.log('Success!');
      },
      (error) => {
        console.error('Error', error);
      }
    )
  }

  listPics() {
    // console.log('Retrieved Stack: ',this.imageStack);
    this.nativeStorage.getItem('myPictures').then(
      (data) => {
        console.log('Retrieved from Native Storage', data);
      },
      (error) => {
        console.error('Error', error);
      }
    )
  }

  fetchImageMeta(id : string = null) {
    // this.processing = true;
    let params : URLSearchParams = new URLSearchParams();
    params.set('id', id);
    params.set('key', 'ADD YOUR KEY');
    params.set('orientation', 'horizontal');
    this.http.get('https://pixabay.com/api', { search : params }).subscribe(
      (data)=>{
        console.log(data.json());
        let json = data.json();
        this.picture = json.hits[0];
        this.processing = false;
        console.log('Retrieved tags', this.picture.tags);
        let keywordArray = this.picture.tags.split(",");
        this.pictureTags = keywordArray;
        console.log('Tags array: ',this.pictureTags);
        this.fetchRelated(this.pictureTags);
        this.displayQuote(this.pictureTags);
        // if(this.picture.tags.lenght > 0){
        //   // this.fetchRelated(this.picture.tags);
        // }
        // this.searchCount = json.totalHits;
        // alert(this.searchCount);
      },
      (error)=> {
        console.error(error);
        // this.errorLoading = true;
        // this.processing = false;
      }
    )
  }

  pickRandomTag(tags : string[]){
    let tag = tags[Math.floor(Math.random() * tags.length)];
    console.log('randomly selected tag : ', tag );
    return tag;
  }

  fetchRelated(related : string[]) {
    // this.processing = true;
    let params : URLSearchParams = new URLSearchParams();
    for(let keyword of related){
        params.append('q', keyword );
    }
    params.set('key', '3200004-a304b17e1b87c5ec537cdf931');
    params.set('orientation', 'horizontal');
    params.set('per_page', '10');
    this.http.get('https://pixabay.com/api', { search : params }).subscribe(
      (data)=>{
        console.log(data.json());
        let json = data.json();
        this.related = json.hits;
        console.log('related images: ', this.related);
      },
      (error)=> {
        console.error(error);
        // this.errorLoading = true;
        // this.processing = false;
      }
    )
  }

  fetchQuote(keyword){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Mashape-Key', 'D7S9SDttBimshkGCpEHdlzXpfcUxp1cBiJejsn6dlBQUUILL84');
    let options = new RequestOptions({headers : headers});
    return this.http.get('https://yusufnb-quotes-v1.p.mashape.com/widget/~+'+keyword+'.json', options);

  }

  displayQuote(tag : string[]){
    this.quotes = {
      quote : 'Fetching....'
    }
    let pickedTag = this.pickRandomTag(tag);
    this.fetchQuote(pickedTag).subscribe(
      (data) => {
        this.quotes = data.json();
        console.log('selected quote: ',this.quotes);
      }
    )
  }

}
