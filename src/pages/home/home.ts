import { Component, ElementRef } from '@angular/core';
import { NavController, Config } from 'ionic-angular';
import { Http, URLSearchParams } from '@angular/http';
import { PicturePage } from '../picture/picture';
import * as Masonry from 'masonry-layout';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  images : any;
  searchCount : number;
  searchTerm : string;
  errorLoading : boolean = false;
  processing : boolean = false;
  pageNumber : number = 1;
  isSearchSet : boolean = false;
  barHidden : boolean = true;
  isDesktop : boolean = false;
  constructor(
    public navCtrl: NavController,
    public http : Http,
    public elementRef : ElementRef,
    public config : Config
  ) {
    // console.log(Masonry);
    this.searchImage();
  }

  ionViewDidLoad(){
    this.isDesktop = this.config.get('isDesktop');
    // console.log(this.config.get('isDesktop'));
  }

  onScrollEnd($event){
    // console.log($event);
    let pos = $event.scrollTop;
    if (pos > 100){
      this.barHidden = !this.barHidden;
      console.log('ScrollTop is '+pos+'. Show Scrollbar. this.barHidden now = '+this.barHidden);
    }
    else {
      console.log('ScrollTop is '+pos+'. Hide Scrollbar. this.barHidden now = '+this.barHidden);
      this.barHidden = true;
    }
  }

  openPic(key: any) {
    this.navCtrl.push(PicturePage, {
      id : key
    });
  }

  setupMasonry(){
    setTimeout(
      ()=>{
        console.log('mason activate');
        // let elmn = document.querySelector('.msn-grid');
        // let elmn : ElementRef = this.elementRef.nativeElement.querySelector('.msn-grid');
        // console.log(elmn);
        var msny = new Masonry('.msn-grid', {
          itemSelector: '.msn-grid-item',
          columnWidth: 200
        })
      }, 3000 );
  }

  doInfinite(infiniteScroll) {
    let maximum = 5;
    this.pageNumber = this.pageNumber + 1;

    if(this.pageNumber < maximum){
      this.fetchStuff().subscribe(
        (data) => {
          let images = data.json();
          for(let item of images.hits){
            // console.log('Extra images : ', item);
            this.images.push(item);
          }
          infiniteScroll.complete();
        },
        (error) => {
          this.pageNumber = this.pageNumber - 1;
          infiniteScroll.complete();
        }
      )
    }

    else {
      infiniteScroll.complete();
      console.log('Reached maximum number of pagination - currently set at ('+maximum+') pages');
    }
  }

  doSearch(keyword : string){
    this.isSearchSet = true;
    this.searchImage(keyword);
  }

  searchImage(keyword : string = null){
    this.processing = true;
    this.fetchStuff(keyword).subscribe(
      (data)=>{
        console.log(data.json());
        let json = data.json();
        this.images = json.hits;
        this.searchCount = json.totalHits;
        this.processing = false;
        this.errorLoading = false;
        // alert(this.searchCount);
        // this.setupMasonry();
      },
      (error)=> {
        console.error(error);
        this.errorLoading = true;
        this.processing = false;
      }
    )
  }

  fetchStuff(keyword : string = null) {
    let params : URLSearchParams = new URLSearchParams();
    if(keyword !== null){
      params.set('q', keyword);
    }
    params.set('page', this.pageNumber.toString());
    params.set('key', 'ADD YOUR KEY');
    params.set('orientation', 'horizontal');
    params.set('per_page', '20');
    params.set('min_height', '350');
    return this.http.get('https://pixabay.com/api', { search : params });
  }

}
