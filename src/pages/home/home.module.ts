import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { ImagePreloaderModule } from '../../components/image-preloader.module';

@NgModule({
  declarations: [
    HomePage
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    ImagePreloaderModule
  ],
})
export class HomePageModule {}
